import React, { Component } from "react"
import './Palette.css'
import ColorBox from "./ColorBox"
import Navbar from "./Navbar";
import PaletteFooter from "./PaletteFooter";

class Palette extends Component {
    constructor(props) {
        super(props);
        this.state = {
            format: 'hex',
            level: 500
        }
    }
    
    changeLevel = (newLevel) => {
        this.setState({
            level: newLevel
        })
    }
    
    changeFormat = (val) => {
        this.setState({
            format: val
        })
    }
    
    render() {
        const { colors, paletteName, emoji, id } = this.props.palette
        const { level, format } = this.state
        const colorBoxes = colors[level].map(color => (
            <ColorBox
                background={ color[format] }
                name={ color.name }
                key={ color.id }
                id={ color.id }
                paletteId={ id }
                showLink
                showingFullPalette
            />
        ))
        return (
            <div className={ 'Palette' }>
                <Navbar level={ level } changeLevel={ this.changeLevel } handleChange={ this.changeFormat }
                        showingAllColors/>
                <div className={ 'Palette-colors' }>
                    { colorBoxes }
                </div>
                <PaletteFooter paletteName={ paletteName } emoji={ emoji }/>
            </div>
        )
    }
}

export default Palette;